<!-- Footer Section -->
<div class="site-footer">
    <!-- Footer Top start -->
    <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="footer-lwf">
                        <h3 class="footer-logo"><img src="images/logo.png" alt="LawFirm"></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce aliquet, massa ac ornare feugiat, nunc dui auctor ipsum, sed posuere eros sapien id quam. </p>
                        <ul class="footer-contact">
                            <li><i class="fa fa-phone"></i> +1 1234 456789</li>
                            <li><i class="fa fa-envelope"></i> info@companyname.com</li>
                            <li><i class="fa fa-fax"></i> +1 123 123456</li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="footer-lwf footer-menu">
                        <h3 class="footer-lwf-title">Quick Links</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="about.html">About Company</a></li>
                            <li><a href="services.html">Our Services</a></li>
                            <li><a href="blog-list.html">Blog</a></li>
                            <li><a href="contact-us.html">Contact Us</a></li>
                            <li><a href="gallery.html">Gallery</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="footer-lwf footer-menu">
                        <h3 class="footer-lwf-title">About Lawyer</h3>
                        <ul>
                            <li><a href="practice-areas.html">Practice Areas List</a></li>
                            <li><a href="practice-area-detail.html">Practice Areas Detail</a></li>
                            <li><a href="attorney-list.html">Attorney List</a></li>
                            <li><a href="attorney.html">Attorney Detail</a></li>
                            <li><a href="faq.html">FAQs</a></li>
                            <li><a href="testimonials.html">Testimonials</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="footer-lwf">
                        <h3 class="footer-lwf-title">Opening Hours</h3>
                        <ul class="open-hours">
                            <li><span>Mon to Fri:</span> <span class="text-right">09:30AM to 05:30PM</span></li>
                            <li><span>Sun:</span> <span class="text-right">Closed</span></li>
                        </ul>
                        <div class="newsletter">
                            <form>
                                <input type="text" placeholder="Enter your email" value="" class="news-input">
                                <button type="submit" value="" class="news-btn"><i class="fa fa-envelope-o" aria-hidden="true"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer top end -->

    <!-- copyright start -->
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-6">Copyright &copy; 2017 <span>LawFirm</span>. All Rights Reserved</div>
                <div class="col-md-12 col-md-6 text-right">Design &amp; Development By: <a href="http://sharjeelanjum.com/" target="_blank">Sharjeel Anjum</a></div>
            </div>
        </div>
    </div>
    <!-- copyright end -->
</div>
