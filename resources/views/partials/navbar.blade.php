<!-- Navigation Section -->
<div class="navbar custom-navbar" role="navigation" id="header">
    <div class="container">

        <!-- NAVBAR HEADER -->
        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="icon icon-bar"></span> <span class="icon icon-bar"></span> <span class="icon icon-bar"></span> </button>
            <!-- lOGO TEXT HERE -->
            <a href="{{ route('index') }}" class="navbar-brand"><img src="images/logo.png" class="whtlogo" alt="" /> <img src="images/logo-color.png" class="logocolor" alt=""></a> </div>

        <!-- NAVIGATION LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="{{ route('index') }}">Inicio</a></li>
                <li><a href="about-us.html">Nosotros</a></li>
                <li><a href="services.html">Servicios</a></li>
                <li class="dropdown"><a href="#">Blog</a></li>
                <li><a href="contact-us.html">Contacto</a></li>
                <li><span class="calltxt"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;(07)272248</span></li>
                <li><span class="calltxt"><i class="fab fa-whatsapp"></i>&nbsp;&nbsp;0984527184</span></li>
            </ul>
        </div>
    </div>
</div>
