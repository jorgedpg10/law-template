<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jus Gentium - @yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="LawFirm One Page HTML Template">
    <meta name="keywords" content="one page, html, template, responsive, business">
    <meta name="author" content="sharjeel anjum">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap css -->
    <link rel="stylesheet" href=" {{ asset('css/bootstrap.min.css') }} ">

    <!-- Fontawesome css -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">


    <!-- Rev Slider css -->
    <link rel="stylesheet" href="{{ asset('js/revolution-slider/css/settings.css') }} ">

    <!-- Magnific-popup css -->
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }} ">

    <!-- Owl Carousel css -->
    <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }} ">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }} ">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }} ">
</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<div class="preloader">
    <div class="cssload-dots">
        <div class="cssload-dot"></div>
        <div class="cssload-dot"></div>
        <div class="cssload-dot"></div>
        <div class="cssload-dot"></div>
        <div class="cssload-dot"></div>
    </div>
</div>

@include('partials.navbar')

@yield('content')

@include('partials.footer')

<!-- Bootstrap -->
<script src="{{ asset('js/jquery-2.1.4.min.js') }} "></script>
<script src="{{ asset('js/bootstrap.min.js') }} "></script>

<!-- Popup -->
<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/magnific-popup-options.js') }} "></script>

<!-- Carousel -->
<script src="{{ asset('js/owl.carousel.js') }} "></script>

<!-- Sticky Header -->
<script src="{{ asset('js/jquery.sticky.js') }} "></script>

<!-- Revolution Slider -->
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/jquery.themepunch.tools.min.js') }} "></script>
<script type="text/javascript" src="{{ asset('js/revolution-slider/js/jquery.themepunch.revolution.min.js') }} "></script>

<!-- Parallax -->
<script src="{{ asset('js/jquery.parallax.js') }} "></script>

<!-- Counter -->
<script src="{{ asset('js/counter.js') }} "></script>
<script src="{{ asset('js/smoothscroll.js') }} "></script>

<!-- Custom -->
<script src="{{ asset('js/custom.js') }} "></script>
</body>
</html>
