@extends('layouts.app')

@section('title', 'Inicio')

@section('content')
    <!-- Revolution slider start -->
    <div class="tp-banner-container" id="slider">
        <div class="tp-banner">
            <ul>
                <li data-slotamount="7" data-transition="fade" data-masterspeed="1000" data-saveperformance="on"><img
                        alt="" src="{{ asset('imgs/slider/pilares-cafe-oscurecido-70.jpg') }}"
                        data-lazyload="{{ asset('imgs/slider/pilares-cafe-oscurecido-70.jpg') }}">
                    <div class="caption lfb large-title tp-resizeme slidertext2" data-x="right" data-y="200"
                         data-speed="600" data-start="1000">Bienvenidos JG Law Firm
                    </div>
                    <div class="caption lfb large-title tp-resizeme slidertext1" data-x="right" data-y="240"
                         data-speed="600" data-start="1600">Una Firma Responsable
                    </div>
                    <div class="caption lfb large-title tp-resizeme slidertext1" data-x="right" data-y="300"
                         data-speed="600" data-start="2200"> "La ley es razón libre de pasión", Sócrates
                    </div>
                    <div class="caption lfb large-title tp-resizeme sliderbtn" data-x="right" data-y="370"
                         data-speed="600" data-start="2800"><a href="#." class="section-btn">Get Started</a></div>
                </li>
                <li data-slotamount="7" data-transition="fade" data-masterspeed="1000" data-saveperformance="on"><img
                        alt="" src="images/slider/dummy.png" data-lazyload="images/slider/banner2.jpg">
                    <div class="caption lfb large-title tp-resizeme slidertext2" data-x="center" data-y="280"
                         data-speed="600" data-start="1000">Welcome to LawFirm Template
                    </div>
                    <div class="caption lfb large-title tp-resizeme slidertext1" data-x="center" data-y="310"
                         data-speed="600" data-start="1600">Attorney Lawyers Template
                    </div>
                    <div class="caption lfb large-title tp-resizeme sliderbtn" data-x="center" data-y="400"
                         data-speed="600" data-start="2200"><a href="#." class="section-btn">Get Started</a></div>
                </li>
            </ul>
        </div>
    </div>
    <!-- Revolution slider end -->

    <!-- About section -->
    <div class="howitwrap" id="about">
        <div class="fullimg" style="background-image:url(images/abogado.jpg)"></div>
        <div class="stcontent">

            <!-- title start -->
            <div class="section-title">
                <h3>Bienvenido a <span>JG Law Firm</span></h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce aliquet, massa ac ornare feugiat, nunc
                    dui auctor ipsum, sed posuere eros sapien id quam. </p>
            </div>
            <!-- title end -->

            <ul class="howlist">
                <!--step 1-->
                <li>
                    <div class="howbox">
                        <div class="iconcircle"><i class="fa fa-university" aria-hidden="true"></i></div>
                        <h4>Derecho Constitucional</h4>
                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidid ut
                            labore</p>--}}
                    </div>
                </li>
                <!--step 1 end-->

                <!--step 2-->
                <li>
                    <div class="howbox">
                        <div class="iconcircle"><i class="fas fa-gavel" aria-hidden="true"></i></div>
                        <h4>Derecho Penal</h4>
                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidid ut
                            labore</p>--}}
                    </div>
                </li>
                <!--step 2 end-->

                <!--step 3-->
                <li>
                    <div class="howbox">
                        <div class="iconcircle"><i class="fa fa-male" aria-hidden="true"></i></div>
                        <h4>Derecho Procesal</h4>
                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidid ut
                            labore</p>--}}
                    </div>
                </li>
                <!--step 3 end-->
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>

    <!-- Counter Section -->
    <div id="counter">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 counter-item">
                    <div class="counterbox">
                        <div class="counter-icon"><i class="fa fa-users" aria-hidden="true"></i></div>
                        <span class="counter-number" data-from="1" data-to="200" data-speed="1000"></span> <span
                            class="counter-text">Clientes satisfechos</span></div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 counter-item">
                    <div class="counterbox">
                        <div class="counter-icon"><i class="fa fa-university" aria-hidden="true"></i></div>
                        <span class="counter-number" data-from="1" data-to="1512" data-speed="2000"></span> <span
                            class="counter-text">Casos de Éxito</span></div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 counter-item">
                    <div class="counterbox">
                        <div class="counter-icon"><i class="fas fa-user-tie" aria-hidden="true"></i></div>
                        <span class="counter-number" data-from="1" data-to="10" data-speed="3000"></span> <span
                            class="counter-text">Años de experiencia</span></div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 counter-item">
                    <div class="counterbox">
                        <div class="counter-icon"><i class="fa fa-trophy" aria-hidden="true"></i></div>
                        <span class="counter-number" data-from="1" data-to="101" data-speed="4000"></span> <span
                            class="counter-text">Awards</span></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Practice Areas section -->
    <div id="practicearea" class="parallax-section">
        <div class="container">
            <!-- Section Title -->
            <div class="section-title">
                <h3>Áreas de <span>Especialidad</span></h3>
                <p>Contamos con títulos de cuarto nivel en las siguientes áreas del derecho.</p>
            </div>
            <div class="row">
                <!-- Service 1 -->
                <div class="col-md-4 col-sm-6">
                    <div class="service-thumb">
                        <div class="thumb-img"><img src="{{ asset('imgs/tipos-derecho/martillo.jpg') }}" alt=""></div>
                        <h4>Derecho constitucional</h4>
                        <p>Estudia las leyes fundamentales de un Estado y singularmente de su constitución.</p>
                    </div>
                </div>

                <!-- Service 2 -->
                <div class="col-md-4 col-sm-6">
                    <div class="service-thumb">
                        <div class="thumb-img"><img src="{{ asset('imgs/tipos-derecho/esposas.jpg') }}" alt=""></div>
                        <h4>Derecho Penal</h4>
                        <p>Regula la potestad punitiva, es decir que regula la actividad criminal dentro de un
                            Estado.</p>
                    </div>
                </div>

                <!-- Service 3 -->
                <div class="col-md-4 col-sm-6">
                    <div class="service-thumb">
                        <div class="thumb-img"><img src="{{ asset('imgs/tipos-derecho/justicia.jpg') }}" alt=""></div>
                        <h4>Derecho Procesal</h4>
                        <p>Es el conjunto de normas que regulan el proceso judicial.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Tagline -->
    <div class="servicesbox bg1">
        <div class="container">
            <div class="section-title">
                <h3>Personal Injury Lawyers</h3>
            </div>
            <div class="ctoggle">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt mauris est, in faucibus
                    dui viverra et. Aliquam finibus vestibulum elit, at pharetra nisl congue vel. Nunc pretium posuere
                    justo pretium fringilla. Sed volutpat risus non rhoncus convallis. Sed fermentum est at hendrerit
                    pellentesque. Mauris nec leo euismod, sagittis mauris in, posuere est...</p>
                <a href="#" class="readmore">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>

    <!-- Team Section -->
    <div id="team" class="parallax-section">
        <div class="container">

            <!-- Dection Title -->
            <div class="section-title">
                <h3>Nuestro<span>Equipo</span></h3>
                <p>Un grupo de excelentes profesionales</p>
            </div>
            <div class="row">
                <!-- team 1 -->
                <div class="col-md-4 col-sm-6">
                    <div class="team-thumb">
                        <div class="thumb-image"><img src="images/team/team-img1.jpg" class="animate" alt=""></div>
                        <h4>Pedro Peralta</h4>
                        <h5>Asistente de Despacho</h5>
                        <ul class="list-inline social">
                            <li><a href="javascript:void(0);" class="bg-twitter"><i class="fab fa-twitter"
                                                                                    aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-facebook"><i class="fab fa-facebook"
                                                                                     aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-linkedin"><i class="fab fa-linkedin"
                                                                                     aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- team 2 -->
                <div class="col-md-4 col-sm-6">
                    <div class="team-thumb">
                        <div class="thumb-image"><img src="images/team/team-img2.jpg" class="animate" alt=""></div>
                        <h4>Juan Gabriel Peralta</h4>
                        <h5>Director General</h5>
                        <ul class="list-inline social">
                            <li><a href="javascript:void(0);" class="bg-twitter"><i class="fab fa-twitter"
                                                                                    aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-facebook"><i class="fab fa-facebook"
                                                                                     aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-linkedin"><i class="fab fa-linkedin"
                                                                                     aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- team 3 -->
                <div class="col-md-4 col-sm-6">
                    <div class="team-thumb">
                        <div class="thumb-image"><img src="images/team/team-img3.jpg" class="animate" alt=""></div>
                        <h4>Juan Alejandro</h4>
                        <h5>Asistente de Despacho</h5>
                        <ul class="list-inline social">
                            <li><a href="javascript:void(0);" class="bg-twitter"><i class="fab fa-twitter"
                                                                                    aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-facebook"><i class="fab fa-facebook"
                                                                                     aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-linkedin"><i class="fab fa-linkedin"
                                                                                     aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- team 4 -->

            </div>
        </div>
    </div>

    <!-- Tagline Section -->
    <div class="taglinewrap">
        <div class="container">
            <h2>Call Today For A FREE Consultation</h2>
            <h3>+1 (123) 456-7890</h3>
            <p>Sed sed neque laoreet, rhoncus libero id, pharetra est. Sed ut neque est. Maecenas et est sagittis,
                mollis risus dignissim, mattis dolor. </p>
            <a href="#"><i class="fa fa-phone" aria-hidden="true"></i> Contact Us Now</a></div>
    </div>

    <!-- FAQ Section -->
    <div id="faqs" class="parallax-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="faqs">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" class=""
                                                               href="#collapse1">Nunc ut erat at massa elementum
                                            tempus.?</a></h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">Integer aliquam sed ante non volutpat. Aenean vitae nulla
                                        varius, dictum nisi non, rhoncus sem. Vivamus vel velit semper, sagittis ante
                                        vel, tempor augue. Proin quis justo auctor, auctor risus vitae, tempor enim.
                                        Aliquam erat volutpat. Phasellus facilisis aliquam eleifend. Donec eget nisl
                                        elementum, luctus velit ut, viverra tellus.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion"
                                                               class="collapsed" href="#collapse2"> Donec lacus sem,
                                            pretium a eros ut?</a></h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body">Integer aliquam sed ante non volutpat. Aenean vitae nulla
                                        varius, dictum nisi non, rhoncus sem. Vivamus vel velit semper, sagittis ante
                                        vel, tempor augue. Proin quis justo auctor, auctor risus vitae, tempor enim.
                                        Aliquam erat volutpat. Phasellus facilisis aliquam eleifend. Donec eget nisl
                                        elementum, luctus velit ut, viverra tellus. <br>
                                        <br>
                                        Aenean id aliquam velit, eget consequat neque. Suspendisse potenti. Praesent id
                                        cursus odio, eget aliquet lectus. Pellentesque id commodo diam. Aliquam in urna
                                        tincidunt, ullamcorper sapien at, imperdiet ex. Mauris laoreet pellentesque mi
                                        quis ornare. Donec non pulvinar nulla. Aenean suscipit tellus ut ex luctus, eu
                                        rhoncus nisi viverra. Curabitur sit amet erat nulla.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion"
                                                               class="collapsed" href="#collapse3"> Donec lacus sem,
                                            pretium a eros ut?</a></h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">Integer aliquam sed ante non volutpat. Aenean vitae nulla
                                        varius, dictum nisi non, rhoncus sem. Vivamus vel velit semper, sagittis ante
                                        vel, tempor augue. Proin quis justo auctor, auctor risus vitae, tempor enim.
                                        Aliquam erat volutpat. Phasellus facilisis aliquam eleifend. Donec eget nisl
                                        elementum, luctus velit ut, viverra tellus. <br>
                                        <br>
                                        Aenean id aliquam velit, eget consequat neque. Suspendisse potenti. Praesent id
                                        cursus odio, eget aliquet lectus. Pellentesque id commodo diam. Aliquam in urna
                                        tincidunt, ullamcorper sapien at, imperdiet ex. Mauris laoreet pellentesque mi
                                        quis ornare. Donec non pulvinar nulla. Aenean suscipit tellus ut ex luctus, eu
                                        rhoncus nisi viverra. Curabitur sit amet erat nulla.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion"
                                                               class="collapsed" href="#collapse4">Nunc ut erat at massa
                                            elementum tempus.?</a></h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body">Integer aliquam sed ante non volutpat. Aenean vitae nulla
                                        varius, dictum nisi non, rhoncus sem. Vivamus vel velit semper, sagittis ante
                                        vel, tempor augue. Proin quis justo auctor, auctor risus vitae, tempor enim.
                                        Aliquam erat volutpat. Phasellus facilisis aliquam eleifend. Donec eget nisl
                                        elementum, luctus velit ut, viverra tellus. <br>
                                        <br>
                                        Aenean id aliquam velit, eget consequat neque. Suspendisse potenti. Praesent id
                                        cursus odio, eget aliquet lectus. Pellentesque id commodo diam. Aliquam in urna
                                        tincidunt, ullamcorper sapien at, imperdiet ex. Mauris laoreet pellentesque mi
                                        quis ornare. Donec non pulvinar nulla. Aenean suscipit tellus ut ex luctus, eu
                                        rhoncus nisi viverra. Curabitur sit amet erat nulla.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion"
                                                               class="collapsed" href="#collapse5"> Donec lacus sem,
                                            pretium a eros ut?</a></h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="panel-body">Integer aliquam sed ante non volutpat. Aenean vitae nulla
                                        varius, dictum nisi non, rhoncus sem. Vivamus vel velit semper, sagittis ante
                                        vel, tempor augue. Proin quis justo auctor, auctor risus vitae, tempor enim.
                                        Aliquam erat volutpat. Phasellus facilisis aliquam eleifend. Donec eget nisl
                                        elementum, luctus velit ut, viverra tellus. <br>
                                        <br>
                                        Aenean id aliquam velit, eget consequat neque. Suspendisse potenti. Praesent id
                                        cursus odio, eget aliquet lectus. Pellentesque id commodo diam. Aliquam in urna
                                        tincidunt, ullamcorper sapien at, imperdiet ex. Mauris laoreet pellentesque mi
                                        quis ornare. Donec non pulvinar nulla. Aenean suscipit tellus ut ex luctus, eu
                                        rhoncus nisi viverra. Curabitur sit amet erat nulla.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    <!-- Video start -->
                    <div class="videowraper">
                        <div class="videobg">
                            <div class="video-image" style="background-image:url(images/videobg.jpg)"></div>
                            <div class="playbtn"><a href="#." class="popup" data-toggle="modal"
                                                    data-target="#watchvideo"><span></span></a></div>
                        </div>
                    </div>
                    <!-- Video end -->

                    <!-- Video Modal -->
                    <div class="modal fade" id="watchvideo" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="video-container">
                                        <iframe width="100%" height="310"
                                                src="https://www.youtube.com/embed/7e90gBu4pas" frameborder="0"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Testimonials Section -->
    <div id="testimonials">
        <div class="container">

            <!-- Section Title -->
            <div class="section-title">
                <h3>Testimonials</h3>
                <p>See What Our Clients Saying!</p>
            </div>
            <ul class="testimonialsList">
                <!-- Client -->
                <li class="item">
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra id nunc at finibus.
                        Etiam sollicitudin faucibus cursus. Proin luctus cursus nulla sed iaculis. Quisque vestibulum
                        augue nec aliquet aliquet."</p>
                    <div class="clientname">Jhon Doe</div>
                    <div class="clientinfo">CEO - Company Inc</div>
                </li>

                <!-- Client -->
                <li class="item">
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra id nunc at finibus.
                        Etiam sollicitudin faucibus cursus. Proin luctus cursus nulla sed iaculis. Quisque vestibulum
                        augue nec aliquet aliquet."</p>
                    <div class="clientname">Jhon Doe</div>
                    <div class="clientinfo">CEO - Company Inc</div>
                </li>

                <!-- Client -->
                <li class="item">
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra id nunc at finibus.
                        Etiam sollicitudin faucibus cursus. Proin luctus cursus nulla sed iaculis. Quisque vestibulum
                        augue nec aliquet aliquet."</p>
                    <div class="clientname">Jhon Doe</div>
                    <div class="clientinfo">CEO - Company Inc</div>
                </li>

                <!-- Client -->
                <li class="item">
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra id nunc at finibus.
                        Etiam sollicitudin faucibus cursus. Proin luctus cursus nulla sed iaculis. Quisque vestibulum
                        augue nec aliquet aliquet."</p>
                    <div class="clientname">Jhon Doe</div>
                    <div class="clientinfo">CEO - Company Inc</div>
                </li>
            </ul>
        </div>
    </div>

    <!-- Blog Section -->
    <div id="blog">
        <div class="container">
            <!-- SECTION TITLE -->
            <div class="section-title">
                <h3>Latest From <span>Blog</span></h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
            <ul class="blogGrid">
                <li class="item">
                    <div class="int">
                        <!-- Blog Image -->
                        <div class="postimg"><img src="images/blog/1.jpg" alt="Blog Title"></div>
                        <!-- Blog info -->
                        <div class="post-header">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> Sep 25, 2017</div>
                            <h4><a href="#.">Duis ultricies aliquet mauris</a></h4>
                            <div class="postmeta">By : <span>Jhon Doe </span> Category : <a href="#.">Job Search </a>
                            </div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus
                            finibus maecenas quis sem ...</p>
                        <a href="#." class="readmore">Read More</a></div>
                </li>
                <li class="item">
                    <div class="int">
                        <!-- Blog Image -->
                        <div class="postimg"><img src="images/blog/2.jpg" alt="Blog Title"></div>
                        <!-- Blog info -->
                        <div class="post-header">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> Sep 25, 2017</div>
                            <h4><a href="#.">Duis ultricies aliquet mauris</a></h4>
                            <div class="postmeta">By : <span>Jhon Doe </span> Category : <a href="#.">Job Search </a>
                            </div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus
                            finibus maecenas quis sem ...</p>
                        <a href="#." class="readmore">Read More</a></div>
                </li>
                <li class="item">
                    <div class="int">
                        <!-- Blog Image -->
                        <div class="postimg"><img src="images/blog/3.jpg" alt="Blog Title"></div>
                        <!-- Blog info -->
                        <div class="post-header">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> Sep 25, 2017</div>
                            <h4><a href="#.">Duis ultricies aliquet mauris</a></h4>
                            <div class="postmeta">By : <span>Jhon Doe </span> Category : <a href="#.">Job Search </a>
                            </div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus
                            finibus maecenas quis sem ...</p>
                        <a href="#." class="readmore">Read More</a></div>
                </li>
                <li class="item">
                    <div class="int">
                        <!-- Blog Image -->
                        <div class="postimg"><img src="images/blog/4.jpg" alt="Blog Title"></div>
                        <!-- Blog info -->
                        <div class="post-header">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> Sep 25, 2017</div>
                            <h4><a href="#.">Duis ultricies aliquet mauris</a></h4>
                            <div class="postmeta">By : <span>Jhon Doe </span> Category : <a href="#.">Job Search </a>
                            </div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus
                            finibus maecenas quis sem ...</p>
                        <a href="#." class="readmore">Read More</a></div>
                </li>
            </ul>
        </div>
    </div>

    <!-- Clients Logo-->
    <div class="our-clients">
        <div class="container">
            <div class="owl-clients">
                <div class="item"><img src="images/client-logo4.png" alt=""></div>
                <div class="item"><img src="images/client-logo.png" alt=""></div>
                <div class="item"><img src="images/client-logo2.png" alt=""></div>
                <div class="item"><img src="images/client-logo3.png" alt=""></div>
                <div class="item"><img src="images/client-logo.png" alt=""></div>
                <div class="item"><img src="images/client-logo3.png" alt=""></div>
                <div class="item"><img src="images/client-logo2.png" alt=""></div>
                <div class="item"><img src="images/client-logo3.png" alt=""></div>
                <div class="item"><img src="images/client-logo.png" alt=""></div>
                <div class="item"><img src="images/client-logo3.png" alt=""></div>
                <div class="item"><img src="images/client-logo2.png" alt=""></div>
            </div>
        </div>
    </div>
    <!-- Clients Logo end-->
@endsection
