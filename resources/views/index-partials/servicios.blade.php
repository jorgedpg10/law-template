<div class="container" role="main">
    <div class="row">
        <div class="col-md-4">
            <div class="panel widget widget_pt_featured_page panel-first-child panel-last-child" id="panel-7-1-0-0">
                <div class="has-post-thumbnail page-box page-box--block">
                    <a class="page-box__picture" href="services-content/design-and-build.html">
                        <img width="360" height="240" src="images/demo/content/content_1.jpg" alt="Content Image"/>
                    </a>
                    <div class="page-box__content">
                        <h5 class="page-box__title  text-uppercase">
                            <a href="design-and-build.html">Design and Build</a>
                        </h5>
                        We aim to eliminate the task of dividing your project between different architecture and construction company. We are a company that offers design and build services for you from initial sketches to the final construction.
                        <p>
                            <a href="services-content/design-and-build.html" class="read-more read-more--page-box">Read more</a>
                        </p>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel widget widget_pt_featured_page panel-first-child panel-last-child" id="panel-7-1-1-0">
                <div class="has-post-thumbnail page-box page-box--block">
                    <a class="page-box__picture" href="services-content/tiling-and-painting.html">
                        <img width="360" height="240" src="images/demo/content/content_2.jpg" alt="Content Image"/>
                    </a>
                    <div class="page-box__content">
                        <h5 class="page-box__title text-uppercase">
                            <a href="tiling-and-painting.html">Tiling and Painting</a>
                        </h5>
                        We offer quality tiling and painting solutions for interior and exterior of residential and commercial spaces that not only looks good but also lasts longer. We offer quality tiling and painting solutions for interior and exterior.
                        <p>
                            <a href="services-content/tiling-and-painting.html" class="read-more read-more--page-box">Read more</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel widget widget_pt_featured_page panel-first-child" id="panel-7-1-2-0">
                <div class="has-post-thumbnail page-box page-box--inline">
                    <a class="page-box__picture" href="services-content/construction-management.html">
                        <img src="images/demo/content/content_mini_1.jpg" alt="Content Image"/>
                    </a>
                    <div class="page-box__content">
                        <h5 class="page-box__title text-uppercase">
                            <a href="services-content/construction-management.html">Construction Management</a>
                        </h5>
                        We offer commitment at all levels of building project, from preparing &hellip;
                    </div>
                </div>
                <div class="spacer"></div>
            </div>
            <div class="panel widget widget_pt_featured_page" id="panel-7-1-2-1">
                <div class="has-post-thumbnail page-box page-box--inline">
                    <a class="page-box__picture" href="services-content/condo-remodeling.html">
                        <img width="100" height="75" src="images/demo/content/content_mini_3.jpg"  class="attachment-thumbnail wp-post-image" alt="Content Image"/>
                    </a>
                    <div class="page-box__content">
                        <h5 class="page-box__title text-uppercase">
                            <a href="services-content/condo-remodeling.html">Condo Remodeling</a>
                        </h5>
                        Our round condo remodelling services includes plumbing, electrical, &hellip;
                    </div>
                </div>
                <div class="spacer"></div>
            </div>
            <div class="panel widget widget_pt_featured_page" id="panel-7-1-2-2">
                <div class="has-post-thumbnail page-box page-box--inline">
                    <a class="page-box__picture" href="services-content/hardwood-flooring.html">
                        <img width="100" height="75" src="images/demo/content/content_mini_2.jpg"  class="attachment-thumbnail wp-post-image" alt="Content Image"/>
                    </a>
                    <div class="page-box__content">
                        <h5 class="page-box__title text-uppercase">
                            <a href="services-content/hardwood-flooring.html">Hardwood Flooring</a>
                        </h5>
                        By hiring our hardwood flooring services, you can transform the &hellip;
                    </div>
                </div>
                <div class="spacer"></div>
            </div>
            <div class="panel widget widget_pt_featured_page panel-last-child" id="panel-7-1-2-3">
                <div class="has-post-thumbnail page-box page-box--inline">
                    <a class="page-box__picture" href="services-content/kitchen-remodeling.html">
                        <img width="100" height="75" src="images/demo/content/content_mini_4.jpg"  class="attachment-thumbnail wp-post-image" alt="Content Image"/>
                    </a>
                    <div class="page-box__content">
                        <h5 class="page-box__title text-uppercase">
                            <a href="services-content/kitchen-remodeling.html">Kitchen Remodeling</a>
                        </h5>
                        We can execute complex kitchen remodelling projects that suit &hellip;
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
