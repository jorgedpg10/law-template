<div class="jumbotron  jumbotron--with-captions">
    <div class="carousel  slide  js-jumbotron-slider" id="headerCarousel" data-interval="5000">
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="images/demo/slider/slider_04.jpg"	alt="The Best Construction HTML Theme" />
                <div class="container">
                    <div class="carousel-content">
                        <div class="jumbotron__category">
                            <h6>HALL OF FAME</h6>
                        </div>
                        <div class="jumbotron__title">
                            <h1>The Best2 Construction HTML Theme</h1>
                        </div>
                        <div class="jumbotron__content">
                            <p>We made just the most advanced and user-friendly construction theme (and probably the only one) in the world.</p>
                            <a class="btn  btn-primary" href="#" target="_blank">BUY THEME</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="images/demo/slider/slider_05.jpg" alt="Construction Management">
                <div class="container">
                    <div class="carousel-content">
                        <div class="jumbotron__category">
                            <h6>WE TAKE CARE OF EVERYTHING</h6>
                        </div>
                        <div class="jumbotron__title">
                            <h1>Construction Management</h1>
                        </div>
                        <div class="jumbotron__content">
                            <p>We provide overall planning, coordination and control of a project, so you don&#8217;t have to worry about anything.</p>
                            <a class="btn  btn-primary" href="our-services" target="_self">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item ">
                <img src="images/demo/slider/slider_01.jpg" alt="Green School in London">
                <div class="container">
                    <div class="carousel-content">
                        <div class="jumbotron__category">
                            <h6>FEATURED PROJECT</h6>
                        </div>
                        <div class="jumbotron__title">
                            <h1>Green School in London</h1>
                        </div>
                        <div class="jumbotron__content">
                            <p>We rebuild entire school center in upper east London in just 2 years. The school is now more efficient then ever before.</p>
                            <a class="btn  btn-primary" href="projects" target="_self">VIEW PROJECT</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="images/demo/slider/slider_07.jpg" alt="Relationships That Last" />
                <div class="container">
                    <div class="carousel-content">
                        <div class="jumbotron__category">
                            <h6>SATISFACTION GUARANTEED</h6>
                        </div>
                        <div class="jumbotron__title">
                            <h1>Relationships That Last</h1>
                        </div>
                        <div class="jumbotron__content">
                            <p>Our promise as a contractor is to build community value into every project while delivering professional expertise.</p>
                            <a class="btn  btn-primary" href="about-us" target="_self">MORE ABOUT US</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="images/demo/slider/slider_03.jpg" alt="Renovating Homes and Offices">
                <div class="container">
                    <div class="carousel-content">
                        <div class="jumbotron__category">
                            <h6>OUR SERVICES</h6>
                        </div>
                        <div class="jumbotron__title">
                            <h1>Renovating Homes and Offices</h1>
                        </div>
                        <div class="jumbotron__content">
                            <p>We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
                            <a class="btn  btn-primary" href="our-services" target="_self">OUR SERVICES</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#headerCarousel" role="button" data-slide="prev">
            <i class="fa fa-angle-left"></i>
        </a>
        <a class="right carousel-control" href="#headerCarousel" role="button" data-slide="next">
            <i class="fa fa-angle-right"></i>
        </a>
    </div>
</div>
