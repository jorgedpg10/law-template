<div class="panel-grid">
    <div class="panel-grid-cell">
        <div class="panel container">
            <h3 class="widget-title">Clients / Partners / Certificates</h3>
            <div class="textwidget">
                <div class="logo-panel">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2"><img src="images/demo/clients/client_01.jpg" alt="Client" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="images/demo/clients/client_02.jpg" alt="Client" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="images/demo/clients/client_03.jpg" alt="Client" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="images/demo/clients/client_04.jpg" alt="Client" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="images/demo/clients/client_05.jpg" alt="Client" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="images/demo/clients/client_06.jpg" alt="Client" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="images/demo/clients/client_07.jpg" alt="Client" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="images/demo/clients/client_08.jpg" alt="Client" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="images/demo/clients/client_09.jpg" alt="Client" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="images/demo/clients/client_10.jpg" alt="Client" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="images/demo/clients/client_11.jpg" alt="Client" width="208" height="98"></div>
                        <div class="col-xs-12 col-sm-2"><img src="images/demo/clients/client_12.jpg" alt="Client" width="208" height="98"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
