<div class="siteorigin-panels-stretch panel-row-style testimonials-bg">
    <div class="container">
        <div class="panel-grid">
            <div class="panel-grid-cell" id="pgc-7-4-0">
                <div class="panel widget widget_pt_testimonials panel-first-child panel-last-child">
                    <div class="testimonial">
                        <h2 class="widget-title">
                            <a class="testimonial__carousel testimonial__carousel--left" href="#carousel-testimonials-widget-4-0-0" data-slide="next">
                                <i class="fa  fa-angle-right" aria-hidden="true"></i>
                                <span class="sr-only" role="button">Next</span>
                            </a>
                            <a class="testimonial__carousel  testimonial__carousel--right" href="#carousel-testimonials-widget-4-0-0" data-slide="prev">
                                <i class="fa  fa-angle-left" aria-hidden="true"></i>
                                <span class="sr-only" role="button">Previous</span>
                            </a>
                            Testimonials
                        </h2>
                        <div id="carousel-testimonials-widget-4-0-0" class="carousel slide">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <div class="row">
                                        <div class="col-xs-12  col-sm-6">
                                            <blockquote class="testimonial__quote">
                                                Our construction managment professionals organize, lead and manage the people, materials
                                                and processes of construction utilizing the latest technologies within the industry. Our
                                                construction management Our construction management.
                                            </blockquote>
                                            <cite class="testimonial__author">Bob the Builder</cite>
                                            <div class="testimonial__rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                        <div class="col-xs-12  col-sm-6">
                                            <blockquote class="testimonial__quote">
                                                We aim to eliminate the task of dividing your project between different architecture and
                                                construction company. We are a company that offers design and build services for you from
                                                initial sketches to the final construction.
                                            </blockquote>
                                            <cite class="testimonial__author">Lennie Lazenby</cite>
                                            <div class="testimonial__rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="row">
                                        <div class="col-xs-12  col-sm-6">
                                            <blockquote class="testimonial__quote">
                                                We offer quality tiling and painting solutions for interior and exterior of residential and
                                                commercial spaces that not only looks good but also lasts longer. We offer quality tiling and
                                                painting solutions for interior and exterior.
                                            </blockquote>
                                            <cite class="testimonial__author">Sandy Beach</cite>
                                            <div class="testimonial__rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                        <div class="col-xs-12  col-sm-6">
                                            <blockquote class="testimonial__quote">
                                                For us, honesty is the only policy and we strive to complete all projects with integrity, not
                                                just with our clients, but also our suppliers and contractors. With thousands of successful
                                                projects under our belt, we are one of the most trusted construction companies in US.
                                            </blockquote>
                                            <cite class="testimonial__author">Dizzy</cite>
                                            <div class="testimonial__rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
