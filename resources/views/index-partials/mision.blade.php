<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel widget widget_text panel-first-child" id="panel-7-3-0-0">
                <div class="textwidget"></div>
            </div>
            <div class="panel panel-grid widget widget_black-studio-tinymce panel-last-child" id="panel-7-3-0-1">
                <h3 class="widget-title">Nuestra Misión</h3>
                <div class="textwidget">
                    <h5>
                        <span style="color: #fcc71f"><br/><span class="icon-container"><span class="fa fa-check"></span></span></span>
                        <span style="color: #333333">WE ARE PASSIONATE</span>
                    </h5>
                    <p>
                        We have a proven record of accomplishment and are a reputable company in the United States. We ensure that all projects
                        are done with utmost professionalism using quality materials while offering clients the support and accessibility.
                    </p>
                    <h5>
                        <span style="color: #fcc71f"><br/><span class="icon-container"><span class="fa fa-check"></span></span></span>
                        HONEST AND DEPENDABLE
                    </h5>
                    <p>
                        For us, honesty is the only policy and we strive to complete all projects with integrity, not just with our clients,
                        but also our suppliers and contractors. With thousands of successful projects under our belt, we are one of the most
                        trusted construction companies in US
                    </p>
                    <h5>
                        <span style="color: #fcc71f"><br/><span class="icon-container"><span class="fa fa-check"></span></span></span>
                        <span style="color: #333333">WE ARE ALWAYS IMPROVING</span>
                    </h5>
                    <p>
                        We commit ourselves to complete all projects within the timeline set with our clients. We use the best of technology and
                        tools to ensure that all jobs are done quickly but also giving attention to details and ensuring everything is done correctly.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel widget widget_text panel-first-child" id="panel-7-3-1-0">
                <div class="textwidget"></div>
            </div>
            <div class="panel panel-grid widget widget_black-studio-tinymce panel-last-child" id="panel-7-3-1-1">
                <h3 class="widget-title">Who We Are?</h3>
                <div class="textwidget">
                    <p>
                        <a href="images/demo/content/content_24.jpg" data-rel="prettyPhoto">
                            <img class="alignleft wp-image-115 size-medium" src="images/demo/content/content_3.jpg" alt="Content Image" width="300" height="168"/>
                        </a>
                        <a href="images/demo/content/content_23.jpg" data-rel="prettyPhoto">
                            <img class="alignleft wp-image-116 size-medium" src="images/demo/content/content_4.jpg" alt="Content Image" width="300" height="168"/>
                        </a>
                    </p>
                    <p>
                        BuildPress Inc traces its roots back to 1989 in Colorado and since then have never looked back. With thousands of
                        successful projects under our belt, we can proudly say that we are one of the most trusted construction companies
                        in Colorado performing both domestic and international construction work. For more than 25 years, Construction has
                        offered a wide range of construction services in Colorado, many other cities of United States and around the world.
                    </p>
                    <p>We strive to maintain the highest standards while exceeding client’s expectations at all levels.</p>
                    <h5><strong><a href="about-us">READ MORE</a></strong></h5>
                </div>
            </div>
        </div>
    </div>
</div>
