#Installation

composer install  
npm install  
cp .env.example .env  
php artisan key:generate  
  
create an empty database  
In the .env file fill in the DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, and DB_PASSWORD  
  
php artisan migrate  