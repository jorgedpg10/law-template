<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () { return view('index'); })->name('index');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
